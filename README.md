## NeuralNetsForHR

En este repositorio se encuentra el código utilizado para el experimento del curso "Análisis y Diseño de Experimentos" de la maestría académica de Ciencias de la Computación de la Universidad de Costa Rica.

Ejecución del código:

1.  Instalar RStudio y los siguientes paquetes: plotly, knitr, ggplot2, dplyr, summarytools, reshape2, pwr2, xtable, car, PMCMR, neuralnet, plyr
2.  El primer paso es entrenar redes neuronales (ver parámetros globales) con el archivo train.R, una vez que se ejecuta este archivo, se puede guardar el ambiente un u archivo llamado 'envDataFinal.RData'.
3.  Ejecutar el archivo reporte.rmd el cual cargará el archivo anteriormente generado y devolverá un pdf o html (dependiendo de la configuración de RStudio). 

